package com.example.onlineexam.cucumber;

import com.example.onlineexam.OnlineExamPortalApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = OnlineExamPortalApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
